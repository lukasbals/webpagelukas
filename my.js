// Page Scroll
$(document).ready(function ($) {
    // $('a[href*=\\#]:not([href=\\#])').click(function () {
    //     if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
    //         || location.hostname === this.hostname) {
    //
    //         var target = $(this.hash);
    //         target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    //         if (target.length) {
    //             $('html,body').animate({
    //                 scrollTop: target.offset().top - $('nav').height() + 1
    //             }, 1000);
    //             return false;
    //         }
    //     }
    // });

// Fixed Nav
    $(window).scroll(function () {
        var scrollTop = $('header').height();
        if ($(window).scrollTop() >= scrollTop) {
            $('nav').css({
                position: 'fixed',
                top: '0',
                left: '0'
            });

            $('.logoLukasBals').addClass('transform');
        }
        if ($(window).scrollTop() < scrollTop) {
            $('nav').removeAttr('style');
            $('.logoLukasBals').removeClass('transform');
        }
    });

    $('.logoLukasBals').click(function () {
        $('html,body').animate({
            scrollTop: 0
        }, 1000);
    });

    $('nav ul li').click(function () {
        var target = $($(this).data('href'));
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - $('nav').height() + 1
            }, 1000);
            return false;
        }
    });
});

// on load screen
$(window).on("load", function () {
    $('body').removeClass('loading').addClass('loaded');
});
